package com.laiter.pockettdemo.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.laiter.pockettdemo.fragments.ContactsFragment;
import com.laiter.pockettdemo.fragments.EmptyFragment;
import com.laiter.pockettdemo.fragments.StoryFragment;

public class MainPagerAdapter extends FragmentPagerAdapter {
    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return ContactsFragment.Companion.create();
            case 1:
                return EmptyFragment.Companion.create();
            case 2:
                return StoryFragment.Companion.create();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
