package com.laiter.pockettdemo.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.laiter.pockettdemo.R
import com.laiter.pockettdemo.pockettAPI.model.Profile
import kotlinx.android.synthetic.main.ticket_contact.view.*

class ContactListAdapter: BaseAdapter {
    var listAdapter = ArrayList<Profile>()
    var context: Context?=null
    constructor(context: Context, contactList:ArrayList<Profile>):super(){
        this.listAdapter = contactList
        this.context = context
    }
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val inflater = LayoutInflater.from(context)
        val ticket = inflater.inflate(R.layout.ticket_contact, null)
        val contact = listAdapter[p0]
        ticket.contact_name.text = contact.last_name + ", " + contact.name
        return ticket
    }

    override fun getItem(p0: Int): Any {
        return  listAdapter[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return listAdapter.size
    }
}