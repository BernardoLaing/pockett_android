package com.laiter.pockettdemo.fragments

import android.support.v4.app.Fragment

class EmptyFragment : Fragment() {
    companion object {

        fun create(): EmptyFragment {
            return EmptyFragment()
        }
    }
}
