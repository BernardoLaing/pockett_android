package com.laiter.pockettdemo.fragments

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.laiter.pockettdemo.R
import com.laiter.pockettdemo.SessionManager
import com.laiter.pockettdemo.adapter.ContactListAdapter
import com.laiter.pockettdemo.pockettAPI.PockettService
import com.laiter.pockettdemo.pockettAPI.ServiceGenerator
import com.laiter.pockettdemo.pockettAPI.model.Profile
import kotlinx.android.synthetic.main.fragment_contacts.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ContactsFragment : BaseFragment() {

    override val layoutResId: Int
        get() = R.layout.fragment_contacts

    override fun inOnCreateView(root: View?, container: ViewGroup?, savedInstances: Bundle?) {
        var contactList: List<Profile>? = null
        var pockettService = ServiceGenerator.createService(PockettService::class.java)
        val call = pockettService.getContacts("Token " + SessionManager.TOKEN)
        call.enqueue(object : Callback<List<Profile>> {
            override fun onFailure(call: Call<List<Profile>>?, t: Throwable?) {
                println("------------- Failure -------------")
            }

            override fun onResponse(call: Call<List<Profile>>?, response: Response<List<Profile>>?) {
                if(response!!.code() == 200){
                    contactList = response.body()
                    println("------------- Success -------------")
                    for(contact in contactList!!){
                        print(contact)
                    }
                }else{
                    print("------------- credential error -------------")
                }
            }

        })
        try {
            val contactArrayList = ArrayList<Profile>(contactList)
            val adapter = ContactListAdapter(activity, contactArrayList)
            contacts_list_view.adapter = adapter
        }catch (ex: Exception){
            Toast.makeText(activity, "Couldn't get contacts", Toast.LENGTH_LONG).show()
        }

    }

    companion object {

        fun create(): ContactsFragment {
            return ContactsFragment()
        }
    }
}
