package com.laiter.pockettdemo.fragments

import android.os.Bundle
import android.view.View
import android.view.ViewGroup

import com.laiter.pockettdemo.R

class StoryFragment : BaseFragment() {
    override val layoutResId: Int
        get() = R.layout.fragment_story

    override fun inOnCreateView(root: View?, container: ViewGroup?, savedInstances: Bundle?) {

    }

    companion object {

        fun create(): StoryFragment {
            return StoryFragment()
        }
    }
}
