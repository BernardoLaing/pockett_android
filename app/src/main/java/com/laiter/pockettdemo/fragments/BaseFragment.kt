package com.laiter.pockettdemo.fragments

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

abstract class BaseFragment : Fragment() {
    private var mRoot: View? = null

    @get:LayoutRes
    abstract val layoutResId: Int

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstances: Bundle?): View? {
        mRoot = inflater!!.inflate(layoutResId, container, false)
        inOnCreateView(mRoot, container, savedInstances)
        return mRoot
    }

    abstract fun inOnCreateView(root: View?, container: ViewGroup?, savedInstances: Bundle?)
}
