package com.laiter.pockettdemo

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.laiter.pockettdemo.adapter.MainPagerAdapter
import com.laiter.pockettdemo.pockettAPI.PockettService
import com.laiter.pockettdemo.pockettAPI.ServiceGenerator
import com.laiter.pockettdemo.pockettAPI.model.Token
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    private lateinit var viewPager: ViewPager
    private var session: SessionManager? = null
    var pockettService: PockettService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        session = SessionManager(applicationContext)

        setContentView(R.layout.activity_main)

        val background = am_background_view
        viewPager = findViewById(R.id.am_view_pager)

        val mainPagerAdapter = MainPagerAdapter(supportFragmentManager)
        viewPager.adapter = mainPagerAdapter

        val snapTabs = am_snap_tabs
        snapTabs.setUpWithViewPager(viewPager)

        viewPager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                if(position == 0){
                    background.background = getDrawable(R.drawable.app_background)
                    background.alpha = 1 - positionOffset
                }else if(position == 1){
                    background.background = getDrawable(R.drawable.app_background)
                    background.alpha =  positionOffset
                }
            }

            override fun onPageSelected(position: Int) {

            }
        })

    }

    override fun onResume() {
        super.onResume()
        if(session!!.checkLogin()){
            //User credentials are null, goes to Login Activity
            finish()
        }
        else{
            //User credentials already stored, create service with Auth Token Header
            pockettService = ServiceGenerator.createService(PockettService::class.java)
        }
    }

    fun logout(view:View){
        session!!.logoutUser()
        finish()
    }

    fun testAuth(view: View){
        println("---------------------------Test Auth-----------------------------------------")
        pockettService = ServiceGenerator.createService(PockettService::class.java)
        val auth = "Token " + SessionManager.TOKEN
        val token = Token(SessionManager.TOKEN)
        println("SessionManager.TOKEN: $auth")
        val call = pockettService!!.testAuth(auth)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                println("------------- Failure -------------")
                Toast.makeText(applicationContext, "Error :(", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                println("onResponse----------------")
                println(response!!.code())
                if(response!!.code() == 200){
                    val result = response.body()
                    println("------------- Success Auth Test -------------")
                    println("result: " + result)
                }else{
                    print("------------- Failure Auth Test -------------")
                    print("status: " + response!!.code())
                }
            }

        })
    }
}
