package com.laiter.pockettdemo.pockettAPI.model

class AuthKey {
    var key:String = ""

    override fun toString(): String {
        return key
    }
}