package com.laiter.pockettdemo.pockettAPI.model

class Token(token: String) {
    var token: String = ""
    init {
       this.token = token
    }

    override fun toString(): String {
        return "Token " + token
    }
}