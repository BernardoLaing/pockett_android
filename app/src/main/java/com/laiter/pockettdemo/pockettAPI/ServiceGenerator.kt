package com.laiter.pockettdemo.pockettAPI

import com.laiter.pockettdemo.SessionManager
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ServiceGenerator {

    val BASE_URL = "http://192.168.1.82:8000/"
    internal var builder: Retrofit.Builder = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())

    internal var retrofit = builder.build()

    private val loggingInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

    private val clientBuilder = OkHttpClient.Builder()

    fun <S> createService(serviceClass: Class<S>): S {
        if (!clientBuilder.interceptors().contains(loggingInterceptor)) {
            clientBuilder.addInterceptor(loggingInterceptor)
            builder = builder.client(clientBuilder.build())
            retrofit = builder.build()
        }

        return retrofit.create(serviceClass)
    }

    fun <S> addInterceptor(interceptor: Interceptor, serviceClass: Class<S>): S{
        if (!clientBuilder.interceptors().contains(loggingInterceptor)) {
            clientBuilder.addInterceptor(loggingInterceptor)
        }
        if(!clientBuilder.interceptors().contains(interceptor)){
            clientBuilder.addInterceptor(interceptor)
            builder = builder.client(clientBuilder.build())
            retrofit = builder.build()
        }

        return retrofit.create(serviceClass)
    }
}
