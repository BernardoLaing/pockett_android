package com.laiter.pockettdemo.pockettAPI.model

class Profile(name:String, last_name:String, contacts:List<String>, profile_picture:String) {
    var name: String = ""
    var last_name: String = ""
    var contacts: List<String>? = null
    var profile_picture: String = ""

    init {
        this.name = name
        this.last_name = last_name
        this.contacts = contacts
        this.profile_picture = profile_picture
    }

    override fun toString(): String {
        return "name: " + name + "-- last_name: " + last_name + "-- contacts: " + contacts + "-- profile_picture: " + profile_picture
    }

}