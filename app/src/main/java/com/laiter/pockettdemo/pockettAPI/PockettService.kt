package com.laiter.pockettdemo.pockettAPI

import com.laiter.pockettdemo.pockettAPI.model.AuthKey
import com.laiter.pockettdemo.pockettAPI.model.Profile
import com.laiter.pockettdemo.pockettAPI.model.User
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface PockettService {
    /*@GET("/2.0/?method=artist.search")
    fun searchArtist(@Query("artist") artist: String): Call<>

    @GET("/2.0/?method=artist.getinfo")
    fun requestArtistInfo(@Query("mbid") id:String, @Query("lang") language : String): Call

    @GET("/2.0/?method=artist.gettopalbums")
    fun requestAlbums(@Query("mbid") id: String, @Query("artist") artist: String): Call

    @GET("/2.0/?method=artist.getsimilar")
    fun requestSimilar(@Query("mbid") id: String): Call

    @GET("/2.0/?method=album.getInfo")
    fun requestAlbum(@Query("mbid") id: String): Call*/

    @POST("rest-auth/login/")
    fun requestLogin(@Body user: User): Call<AuthKey>

    @GET("api_profiles/test_auth/")
    fun testAuth(@Header("Authorization") authToken: String):Call<ResponseBody>

    @GET("api_profiles/contact_list/2/")
    fun getContacts(@Header("Authorization") authToken: String):Call<List<Profile>>
}