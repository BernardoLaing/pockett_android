package com.laiter.pockettdemo.view

import android.animation.ArgbEvaluator
import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewTreeObserver
import android.widget.FrameLayout
import android.widget.ImageView

import com.laiter.pockettdemo.R

class SnapTabsView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : FrameLayout(context, attrs, defStyleAttr), ViewPager.OnPageChangeListener {

    private var mCameraBtn: ImageView? = null
    private var mChatBtn: ImageView? = null
    private var mStoryBtn: ImageView? = null
    private var indicator: View? = null

    private var argbEvaluator: ArgbEvaluator? = null

    private var centerColor: Int = 0
    private var sideColor: Int = 0

    private var indicatorTranslationX: Int = 0

    init {

        init()
    }

    fun setUpWithViewPager(viewPager: ViewPager) {
        viewPager.addOnPageChangeListener(this)
    }

    fun init() {
        LayoutInflater.from(context).inflate(R.layout.view_snap_tabs, this, true)
        mCameraBtn = findViewById(R.id.vst_camera_btn)
        mChatBtn = findViewById(R.id.vst_chat_btn)
        mStoryBtn = findViewById(R.id.vst_story_btn)
        indicator = findViewById(R.id.vst_indicator)

        centerColor = ContextCompat.getColor(context, R.color.white)
        sideColor = ContextCompat.getColor(context, R.color.darkGrey)

        argbEvaluator = ArgbEvaluator()

        mCameraBtn!!.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                mCameraBtn!!.viewTreeObserver.removeOnGlobalLayoutListener(this)
                indicatorTranslationX = mCameraBtn!!.width
            }
        })

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        if (position == 0) {
            moveIndicator(positionOffset - 1)
            scaleIndicator(1 - positionOffset)
        } else if (position == 1) {
            moveIndicator(positionOffset)
            scaleIndicator(positionOffset)
        }

    }

    override fun onPageSelected(position: Int) {

    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    private fun moveIndicator(fromCenter: Float) {
        indicator!!.translationX = fromCenter * indicatorTranslationX
    }

    private fun scaleIndicator(fromCenter: Float) {
        indicator!!.alpha = fromCenter
        indicator!!.scaleX = fromCenter
    }

}
