package com.laiter.pockettdemo

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.laiter.pockettdemo.pockettAPI.PockettService
import com.laiter.pockettdemo.pockettAPI.ServiceGenerator
import com.laiter.pockettdemo.pockettAPI.model.AuthKey
import com.laiter.pockettdemo.pockettAPI.model.User
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.util.HashMap

class SessionManager// Constructor
(// Context
        internal var _context: Context) {
    // Shared Preferences reference
    internal var pref: SharedPreferences

    // Editor reference for Shared preferences
    internal var editor: SharedPreferences.Editor

    // Shared pref mode
    internal var PRIVATE_MODE = 0


    /**
     * Get stored session data
     */
    //Use hashmap to store user credentials
    // user name
    // user email id
    // return user
    val userDetails: HashMap<String, String>
        get() {
            val user = HashMap<String, String>()
            user[KEY_USER] = pref.getString(KEY_USER, null)
            user[KEY_PASS] = pref.getString(KEY_PASS, null)
            user[KEY_AUTH] = pref.getString(KEY_AUTH, null)
            return user
        }


    // Check for login
    val isUserLoggedIn: Boolean
        get() = pref.getBoolean(IS_USER_LOGIN, false)

    init {
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE)
        editor = pref.edit()
        TOKEN = pref.getString(KEY_AUTH, null)
    }

    //Create login session
    fun createUserLoginSession(name: String, password: String, key: String) {
        // Storing login value as TRUE
        editor.putBoolean(IS_USER_LOGIN, true)

        // Storing user in pref
        editor.putString(KEY_USER, name)

        // Storing password in pref
        editor.putString(KEY_PASS, password)

        // Storing Auth Key in pref
        editor.putString(KEY_AUTH, key)

        // commit changes
        editor.apply()

        TOKEN = key

        val i = Intent(_context, MainActivity::class.java)

        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        // Add new Flag to start new Activity
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK

        // Staring Login Activity
        _context.startActivity(i)
    }

    /**
     * Check login method will check user login status
     * If false it will redirect user to login page
     * Else do anything
     */
    fun checkLogin(): Boolean {
        // Check login status
        if (!this.isUserLoggedIn) {

            // user is not logged in redirect him to Login Activity
            val i = Intent(_context, LoginActivity::class.java)

            // Closing all the Activities from stack
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

            // Add new Flag to start new Activity
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK

            // Staring Login Activity
            _context.startActivity(i)

            return true
        }
        //Credentials are already stored, get Auth Key from server
//        var pockettService = ServiceGenerator.createService(PockettService::class.java)
//        val call = pockettService.requestLogin(User(userDetails[KEY_USER]!!, userDetails[KEY_PASS]!!))
//        call.enqueue(object : Callback<AuthKey> {
//            override fun onFailure(call: Call<AuthKey>?, t: Throwable?) {
//                println("------------- Failure -------------")
//            }
//
//            override fun onResponse(call: Call<AuthKey>?, response: Response<AuthKey>?) {
//                if(response!!.code() == 200){
//                    val key = response.body()
//                    println("------------- Success Auto Login -------------")
//                    println("   Key: " + key.toString())
//                    var editor = pref!!.edit()
//                    editor.putString("key", key.toString())
//                    editor.apply()
//
//                    TOKEN = key.toString()
//
//                }else{
//                    print("------------- credential error on auto login -------------")
//                }
//            }
//
//        })
        return false
    }

    /**
     * Clear session details
     */
    fun logoutUser() {

        // Clearing all user data from Shared Preferences
        editor.clear()
        editor.commit()
        TOKEN = ""

        // After logout redirect user to Login Activity
        val i = Intent(_context, LoginActivity::class.java)

        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        // Add new Flag to start new Activity
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK

        // Staring Login Activity
        _context.startActivity(i)
    }

    companion object {

        // Sharedpref file name
        private val PREFER_NAME = "com.laiter.pockettdemo.PREFERENCE_FILE_KEY"

        // All Shared Preferences Keys
        private val IS_USER_LOGIN = "IsUserLoggedIn"

        // User name (make variable public to access from outside)
        val KEY_USER = "user"

        // Password (make variable public to access from outside)
        val KEY_PASS = "pass"

        // Auth Token (make variable public to access from outside)
        val KEY_AUTH = "auth"

        var TOKEN = ""

    }
}
